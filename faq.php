<?php

include "inc/functions.php";

$body = <<<EOT
<style>img{max-width:100%}</style>
<div class="ban">
<h2><center>What is Librechan?</center></h2>
<p>Librechan.net is an imageboard site based off of 8ch, running 'infinity', which is an open source software that allows anyone to create and manage their own anonymous imageboard without any programming or webhosting experience for free.</p>

<h2><center>How is Librechan different?</center></h2>
<p>Unlike 8chan, Librechan is and will remain based off of traditional ideas of freedom of speech, freedom of expression and non-oppressive global and admin moderation.</p>

<h2><center>What is an imageboard?</center></h2>
<p>An imageboard is a type of internet forum which lets users post text and images anonymously (without name) about any topic, or no topic at all. Unlike forums, imageboards do not hold old content permamently, and old threads are pruned as new ones are created.</p>
<p>The imageboard format holds several advantages against traditional forums:</p>
<ol>
<li>There is no registration process, which allows for anyone to post what they like without having to jump through hoops.</li>
<li>Users do not have names and thus feel no reason to build up an identity or reputation for themselves. Post are judged based on their content rather than who made them.</li>
<li>sharing images and multimedia content is as easy as saving and uploading it to the site.</li>
</ol>

<h2><center>How is Librechan run?</center></h2>
<p>Librechan&nbsp;is a service that hosts a large selection of imageboards to browse. These boards are maintained by their respective board owners, who are not affiliated with the Librechan&nbsp;global staff.</p>
<p>The Librechan&nbsp;global staff are responsible for maintaining the site as a whole and protecting it from illegal content. The administration is NOT responsible for enforcing any rules outside of the global rule. Any complaints about the content or management of a board should be addressed towards the owner of the board, unless it violates the law of Netherlands, or global policy.</p>

<h2><center>Are there any global rules regarding content?</center></h2>
<p>Only one:</p>
<ul>
<li>Do not post, request, or link to any content illegal in Netherlands. Do not create boards with the sole purpose of posting or spreading such content.</li>
</ul>
<p><span style="text-decoration: underline;">Any boards or accounts that do not follow this rule will be deleted</span>.</p>
<p>Other than that, you are free to institute whatever rules you want on your board.</p>
<h2><center>How do I create my own board?</center></h2>
<p>To create your own board, go here: <a href="https://librechan.net/create.php">https://librechan.net/create.php</a></p>

<h2><center>I want a board that already exists, can I claim it?</center></h2>
<p>Yes, if a board owner has not logged into his account for 2 weeks, the board will automatically go up for claim. <span style="text-decoration: underline;">Regardless of how much activity the board has!</span><br>To see the full board claim list, go here: <a href="https://librechan.net/claim.html">https://librechan.net/claim.html</a></p>

<h2><center>How do I add more volunteers to my board?</center></h2>
<ul>
<li>1. Find people you can trust.</li>
<li>2. You can add more volunteers in your board settings, click on "Edit board volunteers".</li>
</ul>

<h2><center>How do I manage my board?</center></h2>
<p>Go to <a href="https://librechan.net/mod.php">dashboard</a> or click 'manage' at the top of most librechan web pages.</p>

<h2><center>How do I contact the administrator?</center></h2>
<p>The administrator can be contacted at: <a href="mailto:admin_librechan@openmailbox.org">admin_librechan@openmailbox.org</a>.</p>


<h2><center>How do I post as a volunteer on my board?</center></h2>
<p>Make sure you are using the volunteer interface to view your board. The URL of your browser should be <a href="https://librechan.net/mod.php?/yourboard"><tt>https://librechan.net/mod.php?/yourboard</tt></a>.</p>
<p>If you are the owner of the board, put "## Board Owner" in the name field. If someone else is the owner and you are just assisting them, put "## Board Volunteer" in the name field. Write your post and click "Reply". It will appear with your capcode.</p>

<h2><center>Help! The owner of ____ board is doing something I don't like! Can I have ____ board?</center></h2>
<p>If they aren't doing anything illegal, we can't help you.</p>
<p>If they are, send an email explaining the situation and a link to the board/post to the administrator: <a href="mailto:admin_librechan@openmailbox.org">admin_librechan@openmailbox.org</a></p>

<h2><center>Can you add some new feature?</center></h2>
<p>We are always trying to make improvements to make Librechan better.<br>We would love to hear any of your questions, comments, ideas or recommendations at <a href="https://librechan.net/meta/">https://librechan.net/meta/</a>

<h2><center>What is "sage"?</center></h2>
<p>Posters may reply to threads without bumping them to the top of the index by putting "sage" in the email field.</p>

<h2><center>What is a tripcode?</center></h2>
<p>Most posts on Librechan are made anonymously, but this is not the only way to post. The name field can be used <em>three</em> ways to establish identity:</p>
<ol>
<ol>
<li>By simply writing a name in the box. This is insecure as any other poster can write the same name.</li>
<li>By writing a # character and then a password. Putting #example in the name field would become !KtW6XcghiY. If you want a more customized tripcode, use a software such as Tripcode Explorer. Note that using a tripcode is reasonably secure, but with increasing GPU speeds these tripcodes can be cracked in a few days by a dedicated attacker.</li>
</ol>
</ol>
<p>Board owners and volunteers can enter the special codes "## Board Owner" and "## Board Volunteer" which become <em>capcodes</em> that display after the name. The Librechan administrator can type "## Admin" which becomes <span class="capcode" title="This post was written by the global Librechan administrator."> <i class="fa fa-power-off" style="color:black;"></i> <span style="color:red">Librechan Administrator</span></span>.</p>
<p>Please note, many boards on Librechan have an option set called "Forced anonymity" which causes the name field to not work. This is because many users (and therefore board owners) do not like tripcode users.</p>

<h2><center>How do I format my text?</center></h2>
<p>To format your test, see below:</p>
<ul>
<li>**spoiler** or [spoiler]spoiler[/spoiler] -&gt; spoiler</li>
<li>''italics'' -&gt; <em>italics</em></li>
<li>'''bold''' -&gt; <strong>bold</strong></li>
<li>__underline__ -&gt; <span style="text-decoration: underline;">underline</span></li>
<li>==heading== -&gt; <span class="heading">heading/redtext</span> (must be on its own line)</li>
<li>~~strikethrough~~ -&gt; <s>strikethrough</s></li>
<li>[aa] tags for ASCII/JIS art (escape formatting)</li>
<li>[code] tags if enabled by board owner</li>
<li>$$ and \( \) LaTeX tags (if enabled by board owner)</li>
</ul>

<!-- <h2><center>I would like to send you an encrypted message.</center></h2>
<p>The current admin contact private key can always be found at </p>
<p>The current key fingerprint is <tt></tt>.</p> -->

<h2><center>How do I donate to Librechan?</center></h2>
<p>Donations can be made by bitcoin to the following address: <a href="bitcoin:1LBDSQTJN7LADJRRb8AQNjEj6uKBrihcS9">1LBDSQTJN7LADJRRb8AQNjEj6uKBrihcS9</a></p>
</div>

EOT;

echo Element("page.html", array("config" => $config, "body" => $body, "title" => "FAQ"));

