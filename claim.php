<?php
include 'inc/functions.php';
if (php_sapi_name() == 'fpm-fcgi') {
	error('Cannot be run directly.');
}
function last_activity($board) {
	$ago = (new DateTime)->sub(new DateInterval('P1W'));
	$mod_ago = (new DateTime)->sub(new DateInterval('P2W'));

	$query = query("SELECT id, username FROM mods WHERE boards = '$board' AND type = 20");
	$mods = $query->fetchAll();
	
	if ($mods) {
		$last_activity_date = new DateTime();
		$last_mod_date = new DateTime();
	
		$mod = $mods[0]['id'];
		$query = query("SELECT MAX(time) AS time FROM modlogs WHERE `mod` = $mod");
		$last_mod_activity = $query->fetchAll(PDO::FETCH_COLUMN);
		if ($last_mod_activity[0]) {
			$last_mod_date->setTimestamp($last_mod_activity[0]);	
		} else {
			$last_mod_date = false;
		}
		
		$query = query("SELECT UNIX_TIMESTAMP(time) AS time FROM board_create WHERE uri = '$board'");
		$last_activity = $query->fetchAll(PDO::FETCH_COLUMN);
		if($last_activity[0]) {
			$last_activity_date->setTimestamp($last_activity[0]);
		} else {
			$last_activity_date = false;
		}
		
		
		return ($last_activity_date and $last_activity_date < $ago and $last_mod_date == false) || ($last_mod_date and $last_mod_date < $mod_ago) ? array($last_activity_date, $last_mod_date, $mods) : false;
	} else {
		return false;
	}
}
$q = query("SELECT uri FROM boards WHERE indexed=True OR uri NOT IN (SELECT board FROM overboard_excluded)");
$boards = $q->fetchAll(PDO::FETCH_COLUMN);
$delete = array();
foreach($boards as $board) {
	$last_activity = last_activity($board);

	if ($last_activity) {
		list($last_activity_date, $last_mod_date, $mods) = $last_activity;

		$last_mod_f = $last_mod_date ? $last_mod_date->format('Y-m-d H:i:s') : '<em>never</em>';
		$last_activity_f = $last_activity_date ? $last_activity_date->format('Y-m-d H:i:s') : '<em>never</em>';
		$delete[] = array('board' => $board, 'last_activity_date' => $last_activity_f, 'last_mod' => $last_mod_date, 'last_mod_f' => $last_mod_f);
	}
}
$body = Element("8chan/claim.html", array("config" => $config, "delete" => $delete));
file_write("claim.html", Element("page.html", array("config" => $config, "body" => $body, "title" => _("Claim"), "subtitle" => _("Adopt an abandon board here!"))));
