<?php
// Cronjob every hour:
//
// You will also need the following cronjob asell:
// * */3 * * * cd /var/www/html; /usr/bin/php /var/www/html/tools/rebuild2.php --cache_noboard
//
// Or else ips could still be viewed from global mod pages from cache.
//

include "inc/functions.php";

$boards	= listBoards();

$current_time	= time();

if ($config['clear_ips_posts']) {
	$time_posts   = $current_time - $config['clear_ips_posts'];

	foreach ($boards as &$board) {
		clearIPs($board['uri'], $time_posts);
	}
}

if ($config['clear_ips_modlogs']) {
	$board = false;
	$time_modlogs = $current_time - $config['clear_ips_modlogs'];

	clearIPs($board, $time_modlogs);
}

?>

