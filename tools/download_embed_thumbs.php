<?php
include 'inc/functions.php';

if (php_sapi_name() != 'cli') {
	error('Cannot be run directly.'); 	
}

$boards = listboards();

foreach ($boards as &$board) {

	$query = prepare(sprintf("SELECT `id` FROM ``posts_%s``", $board['uri']));
	$query->execute() or error(db_error($query));
	$ids = $query->fetchAll(PDO::FETCH_COLUMN);

	$board['dir'] = $board['uri'] . "/";

	if (!file_exists($config['dir']['img_root'] . $board['dir'] . $config['dir']['youtube']))
		@mkdir($config['dir']['img_root'] . $board['dir'] . $config['dir']['youtube'], 0777)
			or error("Couldn't create " . $config['dir']['img_root'] . $board['dir'] . $config['dir']['youtube'] . ". Check permissions.", true);


	foreach ($ids as $id) {
		$query = prepare(sprintf("SELECT `embed` FROM ``posts_%s`` WHERE `id` = :id", $board['uri']));
		$query->bindValue(':id', $id, PDO::PARAM_INT);
		$query->execute() or error(db_error($query));

		$post = $query->fetch(PDO::FETCH_ASSOC);

		if ($post['embed']) {
			$youtubeID = preg_replace($config['embedding'][0][0], "$1", $post['embed']);
			$filename = $youtubeID . ".jpg";
			$remote = sprintf($config['youtube_remote_thumb'], $youtubeID);
			
			$path = $config['dir']['img_root'] . $board['dir'] . $config['dir']['youtube'];

			$image = @file_put_contents($path . $filename, file_get_contents($remote));
			if ($image === 0) {
				$data = base64_decode($config['youtube_404_thumb']);

				$im = imagecreatefromstring($data);
				if ($im !== false) {
					imagejpeg($im, $path . $filename);
					imagedestroy($im);
				}
				else {
				    error('An error occurred.');
				}
			}
		}
	}
}

?>
