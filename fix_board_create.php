<?php
	include 'inc/functions.php';
	if (php_sapi_name() != 'cli') {
		error('Cannot be run directly.'); 	
	}

	$query = query("INSERT INTO ``board_create`` (uri) (SELECT `uri` FROM ``boards`` WHERE `uri` NOT IN (SELECT `uri` FROM ``board_create``))");
	$query->execute() or error(db_error($query));

	$now = new DateTime();

	foreach (listBoards() as $val => $board) {
		$first_post = new DateTime();
		$first_mod = new DateTime();
	
		$query = prepare(sprintf("SELECT MIN(time) AS time FROM posts_%s", $board['uri']));
		$query->execute();
		$row = $query->fetch();
		if(isset($row['time']))
			$first_post->setTimestamp($row['time']);
		
		$query = prepare("SELECT `id`, `username` FROM ``mods`` WHERE `boards` = :uri AND `type` = 20");
		$query->bindValue(':uri', $board['uri']);
		$mods = $query->fetchAll();
		if($mods) {
			$mod = $mods[0]['id'];
			$query = query("SELECT MIN(time) AS time FROM ``modlogs`` WHERE `mod` = $mod");
			$query->execute();
			$row = $query->fetch();
			$first_mod->setTimestamp($row['time']);
		}
		
		$time = min($now, $first_post, $first_mod);
		$query = prepare("UPDATE ``board_create`` SET `time` = :time WHERE `uri` = :uri");
		$query->bindValue(':uri', $board['uri']);
		$query->bindValue(':time', $time->format('Y-m-d H:i:s'));
		$query->execute();
		
	}
?>
